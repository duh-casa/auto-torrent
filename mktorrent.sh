#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cd /home/ugowsky/slike
PIDFILE="$SCRIPTPATH/mktorrent.pid"

if [ -e "${PIDFILE}" ]; then
  echo "Already running."
  exit 99
fi

echo $! > $PIDFILE

# preveri če torrenti že obstajajo, če ne jih ustvari in seedaj
for D in *; do
 if [ -d "${D}" ] && [ "${D}" != "peertracker" ]; then
  if [ ! -f "${D}.torrent" ]; then
   echo "${D}"
   transmission-create -o "${D}.torrent" -t "http://rsolj.duh-casa.si/peertracker/announce.php" "${D}"
   transmission-remote -a "${D}.torrent"
  fi
 fi
done

rm $PIDFILE

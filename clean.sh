#!/bin/bash
cd /home/ugowsky/slike
for F in *.torrent; do
 D=${F%.*} 
 if [ ! -d "${D}" ]; then
  HASH=$( transmission-show "${F}" | perl -n -E 'say $1 if /^\s*Hash: (.+)$/' )
  transmission-remote -t $HASH --remove
  rm ${F}
 fi
done
